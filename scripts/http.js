"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const baseUrl = "https://forms-47.herokuapp.com/";
const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjlmMzYwMmM4MjA2YTJlNGQyYTEwNGIiLCJuYW1lIjoiYWpvaG4iLCJlbWFpbCI6ImFqb2huNjRAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTIkN3Ivam9RemhMRFFtLzVkV21PT3BzT2RHQy56QllaSThramlmVGE4c2gvVVlzZGdCTTNTaW0iLCJyb2xlIjoiNjI5OWI0ZmZlM2QyMDA0YzBhNTQ1YzMyIiwiaWF0IjoxNjU0NjAxMjQxLCJleHAiOjE2NTQ2ODc2NDF9.gwJJ2ZvAeoURegX2SjlJPzklIXTQ1v0xOQc6fmXuXPg";
const questionPost = (question) => __awaiter(void 0, void 0, void 0, function* () {
    const postResponse = yield fetch(`${baseUrl}question`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            Authorization: token,
        },
        body: JSON.stringify(question),
    });
    return yield postResponse.json();
});
const allQuestionGet = () => __awaiter(void 0, void 0, void 0, function* () {
    const getResponse = yield fetch(`${baseUrl}question`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: token,
        },
    });
    return yield getResponse.json();
});
const questionDelete = (position) => __awaiter(void 0, void 0, void 0, function* () {
    const deleteResponse = yield fetch(`${baseUrl}question/${position}`, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            Authorization: token,
        }
    });
    return yield deleteResponse.json();
});
const questionPatch = (questionID, shiftOperation) => __awaiter(void 0, void 0, void 0, function* () {
    const patchResponse = yield fetch(`${baseUrl}question/shift/${questionID}`, {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json",
            Authorization: token,
        },
        body: JSON.stringify({ "shiftAction": shiftOperation }),
    });
    return patchResponse.json();
});
