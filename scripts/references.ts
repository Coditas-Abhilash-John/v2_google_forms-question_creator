const addOptions = document.getElementById("add-options-btn") as HTMLButtonElement;
const removeOptions = document.getElementById("remove-options-btn") as HTMLButtonElement;
const optionsContainer = document.getElementById("option-container") as HTMLDivElement;
const questionTypes = document.querySelectorAll(".question-type")!;
const questionSubmit = document.getElementById("submit-btn") as HTMLButtonElement;
const questionEl = document.getElementById("question-id") as HTMLInputElement;
const displayQuestion = document.getElementById("display-question-section") as HTMLDivElement;
const questionTypeIdObj: { [key: string]: string } = {
    "text-question": "6299b4ffe3d2004c0a545c38",
    "numeric-question": "6299b4ffe3d2004c0a545c39",
    "dropdown-question": "6299b4ffe3d2004c0a545c36",
    "checkbox-question": "6299b4ffe3d2004c0a545c37",
  };