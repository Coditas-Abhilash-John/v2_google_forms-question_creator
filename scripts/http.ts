const baseUrl = "https://forms-47.herokuapp.com/";
const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjlmMzYwMmM4MjA2YTJlNGQyYTEwNGIiLCJuYW1lIjoiYWpvaG4iLCJlbWFpbCI6ImFqb2huNjRAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTIkN3Ivam9RemhMRFFtLzVkV21PT3BzT2RHQy56QllaSThramlmVGE4c2gvVVlzZGdCTTNTaW0iLCJyb2xlIjoiNjI5OWI0ZmZlM2QyMDA0YzBhNTQ1YzMyIiwiaWF0IjoxNjU0NjAxMjQxLCJleHAiOjE2NTQ2ODc2NDF9.gwJJ2ZvAeoURegX2SjlJPzklIXTQ1v0xOQc6fmXuXPg"

const questionPost = async (question: questionFormat) => {
  const postResponse = await fetch(`${baseUrl}question`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
    body: JSON.stringify(question),
  });
  return await postResponse.json();
};

interface questionFormat {
  text: string,
  type: string,
  options?: string[],
}

const allQuestionGet = async () => {
  const getResponse = await fetch(`${baseUrl}question`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  return await getResponse.json();
};

const questionDelete = async (position :number) => {
  const deleteResponse = await fetch(`${baseUrl}question/${position}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    }
  });
  return await deleteResponse.json();
};

const questionPatch =async (questionID:string, shiftOperation:string) => {
  const patchResponse = await fetch(`${baseUrl}question/shift/${questionID}`,
  {
    method:"PATCH",
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
    body: JSON.stringify({"shiftAction": shiftOperation}),
})
return patchResponse.json();
} 