addOptions.addEventListener("click", addOptionsFn);
removeOptions.addEventListener("click", removeOptionsFn);

questionTypes.forEach((btn)=>{
    btn.addEventListener('click', ( event)=>{
        return changeQuestionTypeFn(event.target)
    })
});

questionSubmit.addEventListener("click",submitQuestion);