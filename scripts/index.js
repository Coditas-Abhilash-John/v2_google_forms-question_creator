"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let questionType;
const addOptionsFn = () => {
    const optionInputEl = document.createElement("input");
    optionInputEl.classList.add("option-value");
    optionsContainer.append(optionInputEl);
};
const removeOptionsFn = () => {
    optionsContainer.removeChild(optionsContainer.lastChild);
};
const changeQuestionTypeFn = (target) => {
    switch (target.id) {
        case "numeric-question":
        case "text-question":
            optionsContainer.style.display = "none";
            addOptions.style.display = "none";
            removeOptions.style.display = "none";
            break;
        case "dropdown-question":
        case "checkbox-question":
            optionsContainer.style.display = "Block";
            addOptions.style.display = "Block";
            removeOptions.style.display = "Block";
            break;
        default:
            break;
    }
    questionType = target.id + "";
};
const submitQuestion = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const questionStatement = questionEl.value;
        const question = {
            text: questionStatement,
            type: questionTypeIdObj[questionType],
        };
        if (questionType === "dropdown-question" ||
            questionType === "checkbox-question") {
            const optionList = document.querySelectorAll(".option-value");
            const options = [];
            optionList.forEach((el) => {
                options.push(el.value + "");
            });
            question["options"] = options;
        }
        const { data } = yield questionPost(question);
        questionEl.innerHTML = "";
        displayAllQuestion();
        alert(data.message);
    }
    catch (e) {
        console.log(e);
    }
});
const displayAllQuestion = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { data } = yield allQuestionGet();
        const { questions } = data;
        displayQuestion.innerHTML = "";
        questions.forEach((question) => {
            const { text, type, options, position, _id } = question;
            const individualQuestions = document.createElement("div");
            const questionPart = document.createElement("div");
            const questionStat = document.createElement("h5");
            questionStat.innerHTML = text;
            questionPart.append(questionStat);
            const questionTypes = document.createElement("p");
            questionTypes.innerHTML =
                Object.keys(questionTypeIdObj).find((key) => questionTypeIdObj[key] === type) + "";
            questionPart.classList.add("question-part");
            questionPart.append(questionTypes);
            if (options !== null) {
                options.forEach((op) => {
                    const optionsPart = document.createElement("li");
                    optionsPart.innerHTML = op;
                    questionPart.append(optionsPart);
                });
            }
            individualQuestions.classList.add("individual-questions");
            const actionBtnSection = document.createElement("div");
            actionBtnSection.classList.add("action-btn");
            actionBtnSection.style.backgroundColor;
            const verBTn = document.createElement("div");
            verBTn.classList.add("ver-btn");
            const verACtBTn1 = document.createElement("button");
            verACtBTn1.classList.add("ver-act-btn");
            verACtBTn1.innerHTML = ` <span class="material-icons"> arrow_circle_up </span>`;
            verACtBTn1.addEventListener("click", () => shiftQuestion(_id, "UP"));
            verBTn.append(verACtBTn1);
            const verACtBTn2 = document.createElement("button");
            verACtBTn2.classList.add("ver-act-btn");
            verACtBTn2.innerHTML = ` <span class="material-icons"> arrow_circle_down </span>`;
            verACtBTn2.addEventListener("click", () => shiftQuestion(_id, "DOWN"));
            verBTn.append(verACtBTn2);
            actionBtnSection.append(verBTn);
            const delBTn = document.createElement("button");
            delBTn.classList.add("del-btn");
            delBTn.addEventListener("click", () => {
                deleteQuestion(position);
            });
            delBTn.innerHTML = `<span class="material-icons"> delete </span>`;
            actionBtnSection.append(delBTn);
            individualQuestions.append(questionPart);
            individualQuestions.append(actionBtnSection);
            displayQuestion.append(individualQuestions);
        });
    }
    catch (e) {
        console.log(e);
    }
});
displayAllQuestion();
const deleteQuestion = (position) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield questionDelete(position);
        displayAllQuestion();
        alert(`Question at position ${position} deleted ${response}`);
    }
    catch (e) {
        console.log(e);
    }
});
const shiftQuestion = (questionID, shiftOperation) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield questionPatch(questionID, shiftOperation);
        displayAllQuestion();
        console.log(response);
    }
    catch (e) {
        console.log(e);
    }
});
