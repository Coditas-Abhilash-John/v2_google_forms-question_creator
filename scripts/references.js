"use strict";
const addOptions = document.getElementById("add-options-btn");
const removeOptions = document.getElementById("remove-options-btn");
const optionsContainer = document.getElementById("option-container");
const questionTypes = document.querySelectorAll(".question-type");
const questionSubmit = document.getElementById("submit-btn");
const questionEl = document.getElementById("question-id");
const displayQuestion = document.getElementById("display-question-section");
const questionTypeIdObj = {
    "text-question": "6299b4ffe3d2004c0a545c38",
    "numeric-question": "6299b4ffe3d2004c0a545c39",
    "dropdown-question": "6299b4ffe3d2004c0a545c36",
    "checkbox-question": "6299b4ffe3d2004c0a545c37",
};
